import { IPost } from './../models/post';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import * as fromPost from '../store';
import * as postActions from '../store/actions/post.actions';

@Component({
    selector: 'app-posts',
    templateUrl: './posts.component.html',
    styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
    posts$?: Observable<Array<IPost>>;


    constructor(private _store: Store<fromPost.PostState>) { }

    ngOnInit(): void {
        this._store.dispatch(postActions.loadPosts());
        this.posts$ = this._store.pipe(select(fromPost.selectPosts));
    }

}
