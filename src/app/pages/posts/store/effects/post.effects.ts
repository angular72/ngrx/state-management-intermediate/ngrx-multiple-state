import { loadPostsError } from './../actions/post.actions';
import { PostService } from './../../services/post.service';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as postActions from '../actions/post.actions';
import { catchError, map, switchMap } from 'rxjs/operators';
import { IPost } from '../../models/post';
import { of } from 'rxjs';

@Injectable()
export class PostEffects {

    constructor(private _actions$: Actions, private _postService: PostService) {}

    loadPosts$ = createEffect( () =>
        this._actions$.pipe(
            ofType(postActions.loadPosts),
            switchMap( action =>
                this._postService.getPosts().pipe(
                    map( ( posts: Array<IPost> ) => postActions.loadPostsSuccess( { posts } )),
                    catchError( () => of(loadPostsError()))
                )
            )
        )
    );
}
