import { IPost } from './../../models/post';
import { createAction, props } from '@ngrx/store';

export const loadPosts = createAction(
    '[Post] Load Posts'
);

export const loadPostsSuccess = createAction(
    '[Post] Load Posts Success',
    props<{ posts: Array<IPost> }>()
);

export const loadPostsError = createAction(
    '[Post] Load Posts Error'
);

