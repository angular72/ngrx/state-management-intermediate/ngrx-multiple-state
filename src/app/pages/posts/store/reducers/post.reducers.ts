import { createReducer, on } from '@ngrx/store';
import * as postsActions from '../actions/post.actions';
import { IPost } from '../../models/post';

export const postFeatureKey = 'posts';

export interface PostState {
    posts: Array<IPost>;
}

export const initialState: PostState = {
    posts: []
};

export const postsReducer = createReducer(
    initialState,
    on(postsActions.loadPosts, ( state ) => {
        return { ...state };
    }),
    on(postsActions.loadPostsSuccess, ( state, { posts } ) => {
        return { ...state, posts };
    }),
    on(postsActions.loadPostsError, ( state ) => {
        return { ...state };
    }));
