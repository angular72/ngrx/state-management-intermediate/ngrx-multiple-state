import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromPost from '../reducers/post.reducers';

const selectPostState = createFeatureSelector<fromPost.PostState>(fromPost.postFeatureKey);

const _selectPosts = ( postState: fromPost.PostState ) => postState.posts;

export const selectPosts = createSelector(selectPostState, _selectPosts);
