import { IPost } from '../models/post';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class PostService {
    API_BASE_URL = environment.API_BASE_URL;

    constructor(private _http: HttpClient) {}

    getPosts(): Observable<Array<IPost>> {
        return this._http.get<Array<IPost>>(`${this.API_BASE_URL}/posts`);
    }
}
