import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostsRoutingModule } from './posts-routing.module';
import { PostsComponent } from './components/posts.component';

import * as fromPost from './store';
import { PostItemComponent } from './components/post-item/post-item.component';


@NgModule({
  declarations: [PostsComponent, PostItemComponent],
  imports: [
    CommonModule,
    PostsRoutingModule,
    StoreModule.forFeature(fromPost.postFeatureKey, fromPost.postsReducer),
    EffectsModule.forFeature([fromPost.PostEffects])
  ]
})
export class PostsModule { }
