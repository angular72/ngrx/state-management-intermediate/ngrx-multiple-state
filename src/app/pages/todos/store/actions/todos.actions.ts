import { createAction, props } from '@ngrx/store';
import { ITodo } from '../../models/todo';

export const loadTodos = createAction(
    '[Todos] Load Todos'
);

export const loadTodosSuccess = createAction(
    '[Todos] Load Todos Success',
    props<{ todos: Array<ITodo> }>()
);

export const loadTodosError = createAction(
    '[Todos] Load Todos Error'
);

export const completeTodo = createAction(
    '[Todos] Complete Todo',
    props<{ todoId: number }>()
);
