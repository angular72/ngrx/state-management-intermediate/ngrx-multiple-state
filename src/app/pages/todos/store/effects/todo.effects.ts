import { switchMap, map, catchError } from 'rxjs/operators';
import { TodoService } from './../../services/todo.service';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { ITodo } from '../../models/todo';
import * as todoActions from '../actions/todos.actions';
import { of } from 'rxjs';

@Injectable()
export class TodoEffects {
    constructor(
        private _actions$: Actions,
        private _todoService: TodoService
    ) { }

    loadTodos$ = createEffect( () =>
        this._actions$.pipe(
            ofType(todoActions.loadTodos),
            switchMap( action => this._todoService.getTodos().pipe(
                map( ( todos: Array<ITodo> ) => todoActions.loadTodosSuccess( { todos } )),
                catchError( () => of(todoActions.loadTodosError()))
                )
            )
        )
    );
}
