import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromTodos from '../reducers/todos.reducers';

const selectTodoState = createFeatureSelector<fromTodos.TodoState>(fromTodos.todosFeatureKey);

const _selectTodos = ( todoState: fromTodos.TodoState ) => todoState.todos;

export const selectTodos = createSelector(selectTodoState, _selectTodos);
