import { createReducer, on } from '@ngrx/store';
import { ITodo } from '../../models/todo';
import * as todosActions from '../actions/todos.actions';

export const todosFeatureKey = 'todos';

export interface TodoState {
    todos: Array<ITodo>;
}

const initialState: TodoState = {
    todos: []
};

export const todosReducer = createReducer(
    initialState,
    on(todosActions.loadTodos, ( state ) => {
        return { ...state };
    }),
    on(todosActions.loadTodosSuccess, ( state, { todos } ) => {
        return { todos };
    }),
    on(todosActions.loadTodosError, ( state ) => {
        return { ...state };
    }),
    on(todosActions.completeTodo, ( state, { todoId } ) => {
        return {
            todos: state.todos.map( ( todo: ITodo ) =>
                todo.id === todoId
                    ? { ...todo, completed: !todo.completed }
                    : todo
            )
        };
    })
);
