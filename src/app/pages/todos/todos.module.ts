import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TodosRoutingModule } from './todos-routing.module';
import { TodosComponent } from './components/todos.component';
import { TodoItemComponent } from './components/todo-item/todo-item.component';
import * as fromTodos from './store/index';

@NgModule({
  declarations: [TodosComponent, TodoItemComponent],
  imports: [
    CommonModule,
    TodosRoutingModule,
    StoreModule.forFeature(fromTodos.todosFeatureKey, fromTodos.todosReducer),
    EffectsModule.forFeature([fromTodos.TodoEffects])
  ]
})
export class TodosModule { }
