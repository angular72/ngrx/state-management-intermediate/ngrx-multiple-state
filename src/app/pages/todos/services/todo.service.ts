import { ITodo } from './../models/todo';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class TodoService {
    private API_BASE_URL = environment.API_BASE_URL;

    constructor(private _http: HttpClient) { }

    getTodos(): Observable<Array<ITodo>> {
        return this._http.get<Array<ITodo>>(`${this.API_BASE_URL}/todos`);
    }
}
