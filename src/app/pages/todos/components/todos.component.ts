import { ITodo } from './../models/todo';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { Component, OnInit } from '@angular/core';
import * as fromTodos from '../store/index';
import * as todoActions from '../store/actions/todos.actions';

@Component({
    selector: 'app-todos',
    templateUrl: './todos.component.html',
    styleUrls: ['./todos.component.scss']
})
export class TodosComponent implements OnInit {
    todos$?: Observable<Array<ITodo>>;

    constructor(private _store: Store<fromTodos.TodoState>) { }

    ngOnInit(): void {
        this._store.dispatch(todoActions.loadTodos());
        this.todos$ = this._store.pipe(select(fromTodos.selectTodos));
    }

    onComplete(todoId: number): void {
        this._store.dispatch(todoActions.completeTodo( { todoId } ));
    }
}
