import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'posts'
    },
    {
        path: 'posts',
        loadChildren: () => import('./pages/posts/posts.module').then( m => m.PostsModule)
    },
    {
        path: 'todos',
        loadChildren: () => import('./pages/todos/todos.module').then( m => m.TodosModule)
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
