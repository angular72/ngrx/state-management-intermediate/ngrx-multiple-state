export interface AppState {
    [title: string]: string;
}
