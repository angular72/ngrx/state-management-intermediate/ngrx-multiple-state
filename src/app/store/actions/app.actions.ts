import { createAction, props } from '@ngrx/store';

export const initTitle = createAction(
    '[Title] Init Title',
    props<{ title: string }>()
);
