import { createReducer, on } from '@ngrx/store';
import * as AppActions from '../actions/app.actions';
import { AppState } from '../models/app-state';


export const appFeatureKey = 'app';

export const initialState: AppState = {
    title: '',
};

export const appReducer = createReducer(
    initialState,
    on(AppActions.initTitle, ( state, { title } ) => {
        return { ...state, title };
    })
);

