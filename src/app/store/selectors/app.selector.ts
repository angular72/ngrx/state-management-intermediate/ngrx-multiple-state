import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AppState } from '../models/app-state';
import * as fromApp from '../reducers/app.reducer';

const selectAppState = createFeatureSelector<AppState>(fromApp.appFeatureKey);

export const selectTitle = createSelector(selectAppState, ( state: AppState ) => state.title);
