import { initTitle } from './store/actions/app.actions';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import * as fromApp from './store';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    title$?: Observable<string>;

    constructor(private _store: Store<fromApp.AppState>) { }

    ngOnInit(): void {
        this.title$ = this._store.pipe(select(fromApp.selectTitle));
        this._store.dispatch(initTitle( { title: 'Demo of multiple state NgRx App !' } ));
    }
}
